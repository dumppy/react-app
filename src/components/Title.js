import React from 'react'
import { makeStyles } from "@material-ui/core/styles";
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
   root:{
       width:"100%",
       height:80,
       position:'relative'
   },
    title:{
        position:"relative",
        paddingBottom:15,
        '&:before':{
            position:'absolute',
            left:0,
            bottom:0,
            width:100,
            height:7,
            backgroundColor:"rgba(3,127,255,.3)",
            content:"''",
            borderRadius:10
        },
        "&:after":{
            position:'absolute',
            left:0,
            bottom:0,
            width:40,
            height:7,
            backgroundColor:"rgba(3,127,255,1)",
            content:"''",
            borderRadius:10

        },
       
    },
    subtitle1:{
        fontSize:50,
        color:"#ffffff22",
        position:"absolute",
        bottom:-20,
        left:25
    }

}));
export default function Title({txt}) {
    const classes=useStyles()
    return (
        <div className={classes.root} >
            <Typography variant="h2" className={classes.title} >{txt}</Typography>
    <span  className={classes.subtitle1} >{txt}</span>
            </div>
    )
}
