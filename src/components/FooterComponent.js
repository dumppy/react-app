import React from "react";
import {Link} from "react-router-dom";

export default function FooterComponent() {
    return (
        <>
            <div className="row navigation-bottom-wrapper d-flex align-items-center justify-content-center">
                <div
                    className="col py-3 d-flex flex-column justify-content-center align-items-center navigation-bottom">
                    <a href="#" className="align-items-center justify-content-center" style={{display: "block"}}>
                        <i className="fal fa-home-alt d-flex align-items-center justify-content-center bottom-navigation-icon"
                           style={{display: "block"}}></i>داشبورد</a>
                </div>
                <div
                    className="col py-3 d-flex flex-column justify-content-center align-items-center navigation-bottom">
                    <a href="#" className="align-items-center justify-content-center" style={{display: "block"}}><i
                        className="fal fa-archive d-flex align-items-center justify-content-center bottom-navigation-icon"
                        style={{display: "block"}}></i>محصولات</a>
                </div>
                <div
                    className="col py-3 d-flex flex-column justify-content-center align-items-center navigation-bottom">
                    <a href="#" className="align-items-center justify-content-center" style={{display: "block"}}><i
                        className="fal fa-list-alt d-flex align-items-center justify-content-center bottom-navigation-icon"
                        style={{display: "block"}}></i>سفارشات</a>
                </div>
                <div
                    className="col py-3 d-flex flex-column justify-content-center align-items-center navigation-bottom">
                    <a href="#" className="align-items-center justify-content-center" style={{display: "block"}}><i
                        className="fal fa-plus-square d-flex align-items-center justify-content-center bottom-navigation-icon"
                        style={{display: "block"}}></i>جدید</a>
                </div>
                <div
                    className="col py-3 d-flex flex-column justify-content-center align-items-center navigation-bottom">
                    <a href="#" className="align-items-center justify-content-center" style={{display: "block"}}><i
                        className="fal fa-user-alt d-flex align-items-center justify-content-center bottom-navigation-icon"
                        style={{display: "block"}}></i>پروفایل</a>
                </div>
            </div>
        </>
    )
}