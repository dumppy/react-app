import React, {useState, useRef, useEffect} from 'react'
import {makeStyles, useTheme} from "@material-ui/core/styles";
import {BrowserRouter as Router, Switch, Route, Link, useParams, withRouter, Redirect} from "react-router-dom";
import '../screens/home.css'
import '../assets/css/bootstrap.min.css'
import '../assets/css/owl.carousel.css'
import '../assets/css/owl.theme.default.css'
import '../assets/css/market.css'
import '../assets/css/all.min.css'
import '../assets/css/style.css'
import Modal from "@material-ui/core/Modal";
import {Home} from "@material-ui/icons";
import axios from "axios";

const useStyles = makeStyles(theme => ({
    root: {
        /*    display: "flex",
            justifyContent: "center",
            alignItems: "center",
            overflow: 'hidden'*/
    },
}))

export default function NavBarComponent() {
    const classes = useStyles();
    const theme = useTheme();
    const [open, setOpen] = useState(false);
    const node = useRef();
    const token = localStorage.getItem('access_token')

    function openNav() {
        document.getElementById("mySidenav").style.width = "75%";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }

    function logout() {

        axios({
            method: 'POST', url: 'http://elanza.ms/public/api/v1/users/logout', headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token
            },
        })
            .then(res => {
                console.log(res.data.status, res.data.data.token)
            })
            .catch(error => {
                if (error.response) {
                    console.log(error.response, error.response.status)
                }

            })
        localStorage.setItem('access_token',null);
        window.location.href = '/login';
    }

    useEffect(() => {
        // add when mounted
        document.addEventListener("mouseup", handleClick);
        // return function to be called when unmounted
        return () => {
            document.removeEventListener("mouseup", handleClick);
        };
    }, []);
    const handleClick = e => {

        if (node.current.contains(e.target)) {
            openNav();
            return;
        }
        closeNav()
    };
    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    return (
        <div className="col-12 d-flex justify-content-between align-items-center border-bottom py-3">
            <div className="header-icons">
                <i ref={node} className="fal fa-bars" onClick={() => setOpen(true)}></i>

                <div id="mySidenav" className="sidenav">
                    <Link to="/">{'خانه'} </Link>
                    <Link to="/">{'درباره ما'} </Link>
                    <Link to="/">{' تماس با ما'} </Link>
                    <Link to="/" onClick={logout}>{'خروج'} </Link>
                </div>
            </div>
            <div>
                <img src={require('../assets/img/logo-70-70.png')} className="img-fluid w-100"/>
            </div>
            <div className="header-icons">
                <a href="#" className="search">
                    <i className="fal fa-search"></i>
                </a>
            </div>
        </div>
    );


}
