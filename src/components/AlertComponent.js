import Alert from "@material-ui/lab/Alert/Alert";
import React from "react";

export default function alert(code, message) {

    switch (code) {
        case true:
            return <Alert severity="success"> {message} </Alert>;
        case false:
            return <Alert severity="error">{message} </Alert>;
        case 'captcha-not-valid':
            return <Alert severity="error"> کد امنیتی، اشتباه است . </Alert>;
        case 'email-not-verified':
            return <Alert severity="error"> ایمیل درست نیست . </Alert>;
        case 429:
            return <Alert severity="error"> درخواست شما بیش از حد مجار است . </Alert>;
    }
}