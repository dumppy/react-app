import React, { useEffect, useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import axios from 'axios'
export default function ComboBox() {
    const [data, setData] = useState([])
    const controller = new AbortController()
    useEffect(() => {
        const data = {
            email: "",
            password: ""
        }
        //fetch('http://88.198.75.32:8000/api/v1/statics/practice-areas', {
        //    method: 'GET',
        //    //signal: controller.signal,
        //    headers: {
        //       
        //        Accept: 'application/json',
        //        'Content-Type': 'application/json',
        //       'X-Api-Key':'vTVVheKGlNb1xRPKnFSa7P1NLOaiwc7Var6U3o5a',
        //        'X-Localization':'en'
        //     },
        //body: JSON.stringfy(data)
        // }).then((response) => Promise.all([response,response.json()])).then(reponseJson => {
        //     console.log(reponseJson.results)
        //setData(reponseJson.results)
        // })
        //setTimeout(() => { controller.abort() }, 3000)


        axios({
            url: 'https://swapi.co/api/people',
            method: 'GET',
            timeout: 3000,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            data: data
        }).then(response =>
            {
                if(response.status===200){
                    setData(response.data && response.data.results?response.data.results:[])
                }
            })
    }, [])
    return (
        <Autocomplete
            id="combo-box-demo"
            options={data}
            getOptionLabel={option => option.name}
            style={{ width: 300 }}
            renderInput={params => (
                <TextField {...params} label="Combo box" variant="outlined" fullWidth />
            )}
        />
    );
}

