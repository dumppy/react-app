import {createMuiTheme} from '@material-ui/core/styles'
import palette from './palette'
import {getDirection,getFont} from '../locale/index'
const theme=createMuiTheme({
    palette:palette,
    direction:getDirection(),
    spacing:3,
    typography:{
        h1:{
            fontWeight:700,
            fontSize:"3.2857rem",
            lineHeight:"4rem",
            fontFamily:getFont()
        },
        h2:{
            fontWeight:700,
            fontSize:42,
            lineHeight:"3.2857rem",
            fontFamily:getFont()
        },
        h3:{

        },
        h4:{
            fontSize:"1.71rem",
            lineHeight:"2.43rem",
            fontFamily:getFont()
        },
        h5:{

        },
        h6:{
            fontSize:"1.14rem",
            lineHeight:"1.857rem",
            fontFamily:getFont()
        },
        body1:{
            fontSize:18,
            lineHeight:'1.8rem',
            fontFamily:getFont()
        },
        body2:{

        },
        button:{
            fontFamily:getFont()
        },
        caption:{

        },
        subtitle1:{
            fontSize:"1.25rem",
            lineHeight:"2rem",
            fontFamily:getFont()
        },
        subtitle2:{

        }
    }
})
export default theme