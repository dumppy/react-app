const palette={
    primary:{
        main:"#037fff"
    },
    secondary:{
        main:"#fff"
    },
    text:{
        primary:"#FFF",
        secondary:"#a4acc4"
    }
}
export default palette