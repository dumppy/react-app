import React, {createContext, useContext, useState} from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import './assets/css/iransans.css'
import {getDirection} from './locale/index'
import userContext, {userDefault, UserProvider} from '../src/context/user'

document.getElementsByTagName('body')[0].setAttribute('dir', getDirection())

ReactDOM.render(
    <UserProvider value={userDefault}>
        <App/>
    </UserProvider>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
