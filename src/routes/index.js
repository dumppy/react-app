import React from "react";
import {BrowserRouter as Router, Switch, Route, Link, useParams, useRouteMatch, Redirect} from "react-router-dom";
import Home from "../screens/HomeScreen";
import About from "../screens/AboutScreen";
import Contact from "../screens/ContactScreen";
import Login from "../screens/LoginScreen";
import ResetPassword from '../screens/ResetPasswordScreen';
import Verify from '../screens/VerifyScreen';
import Register from '../screens/RegisterScreen';

// This site has n pages, all of which are rendered
// dynamically in the browser (not server rendered).
//
// Although the page does not ever refresh, notice how
// React Router keeps the URL up to date as you navigate
// through the site. This preserves the browser history,
// making sure things like the back button and bookmarks
// work properly.

export default function BasicExample() {

    const token = localStorage.getItem('access_token')
    console.log(token, token !== 'null')

    function PrivateRoute({children, ...rest}) {
        return (
            <Route
                {...rest}
                render={({location}) =>
                    (token !== 'null') ? (
                        children
                    ) : (
                        <Redirect
                            to={{
                                pathname: "/login",
                                state: {from: location}
                            }}
                        />
                    )
                }
            />
        );
    }

    function PublicRoute({children, ...rest}) {
        return (
            <Route
                {...rest}
                render={({location}) =>
                    (!token) ? (
                        children
                    ) : (
                        <Redirect
                            to={{
                                pathname: "/ ",
                                state: {from: location}
                            }}
                        />
                    )
                }
            />
        );
    }

    return (
        <Router>
            {/*  <ul>
                <li>
                  <Link to="/">Home</Link>
                </li>
                <li>
                  <Link to="/about">About</Link>
                </li>
                <li>
                  <Link to="/contact">contact</Link>
                </li>
                <li>
                  <Link to="/login">login</Link>
                </li>
              </ul>*/}

            {/*  <Switch>
                <Route exact path="/">
                    <Home/>
                </Route>
                <Route exact path="/login">
                    <Login/>
                </Route>

                <Route path="/about/:name">
                    <About/>
                </Route>

                <Route exact path="/contact">
                    <Contact/>
                </Route>
                <Route exact path="/reset-password">
                    <ResetPassword/>
                </Route>
                <Route exact path="/verify">
                    <Verify/>
                </Route>
                <Route exact path="/topics">
                    <Topics/>
                </Route>
                <Route component={Contact}  />
            </Switch>*/}
            <Switch>

                <Route exact path="/login">
                    <Login/>
                </Route>

                <Route path="/reset-password">
                    <ResetPassword/>
                </Route>
                <Route path="reset-password/verify">
                    <Verify/>
                </Route>

                <Route path="/register">
                    <Register/>
                </Route>
                <Route path="register/verify">
                    <Verify/>
                </Route>
                <Route path="/products">
                    <Topics/>
                </Route>
                <PrivateRoute path="/">
                    <Home/>
                </PrivateRoute>

            </Switch>
        </Router>
    );

    function Topics() {
        let match = useRouteMatch();

        return (
            <div>
                <h2>Topics</h2>

                <ul>
                    <li>
                        <Link to={`${match.url}/components`}>Components</Link>
                    </li>
                    <li>
                        <Link to={`${match.url}/props-v-state`}>
                            Props v. State
                        </Link>
                    </li>
                </ul>

                {/* The Topics page has its own <Switch> with more routes
          that build on the /topics URL path. You can think of the
          2nd <Route> here as an "index" page for all topics, or
          the page that is shown when no topic is selected */}
                <Switch>
                    <Route path={`${match.path}/:topicId`}>
                        <Topic/>
                    </Route>
                    <Route path={match.path}>
                        <h3>Please select a topic.</h3>
                    </Route>
                </Switch>
            </div>
        );
    }

    function Topic() {
        let {topicId} = useParams();
        return <h3>Requested topic ID: {topicId}</h3>;
    }
}

