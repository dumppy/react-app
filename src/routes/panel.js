import React from "react";
import { BrowserRouter as Router, Switch, Route, Link,useRouteMatch } from "react-router-dom";
import Home from "../screens/HomeScreen";
import About from "../screens/AboutScreen";
import Contact from "../screens/ContactScreen";
// This site has 3 pages, all of which are rendered
// dynamically in the browser (not server rendered).
//
// Although the page does not ever refresh, notice how
// React Router keeps the URL up to date as you navigate
// through the site. This preserves the browser history,
// making sure things like the back button and bookmarks
// work properly.

export default function BasicExample() {
    let { path, url } = useRouteMatch();
  return (
    <Router>
      

      <Switch>
        <Route exact path={path}>
          <Home />
        </Route>

        <Route exact path={`${path}/changePassword`}>
          <Home />
        </Route>
       
      </Switch>
    </Router>
  );
}
