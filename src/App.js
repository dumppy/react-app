import React, {createContext, useState} from 'react';
import {
    ThemeProvider,
    StylesProvider,
    jssPreset
} from "@material-ui/core/styles";
import theme from "./theme/theme";
import Index from "./screens/index";
import {getDirection} from "./locale/index";
import {create} from 'jss';
import rtl from "jss-rtl";
import Routes from './routes/index'
import {UserProvider, useUser} from "./context/UserProvider";


const jss = create({plugins: [...jssPreset().plugins, rtl()]});

function App() {
    return getDirection() === "ltr" ? (
        <UserProvider>
            <ThemeProvider theme={theme}>
                <StylesProvider>
                    <Routes/>
                </StylesProvider>
            </ThemeProvider>
        </UserProvider>
    ) : (
        <UserProvider>
            <ThemeProvider theme={theme}>
                <StylesProvider jss={jss}>
                    <Routes/>
                </StylesProvider>
            </ThemeProvider>
        </UserProvider>
    );
}

export default App;
