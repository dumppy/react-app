import React, {useEffect, useState} from 'react'
import {makeStyles} from "@material-ui/core/styles";
import Typography from '@material-ui/core/Typography';
import TelegramIcon from '@material-ui/icons/Telegram';
import IconButton from '@material-ui/core/IconButton';
import Title from '../components/Title'
import MyAutoComplete from '../components/MyAutoComplete'

import {useParams, withRouter, Redirect} from 'react-router-dom'
import NavBarComponent from "../components/NavBarComponent";
import FooterComponent from "../components/FooterComponent";
import axios from "axios";

const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        flexDirection: "column",
        width: "100%",
        height: "100vh",
        justifyContent: "center",
        alignItems: "center",
        overflow: 'hidden'
    },
    name: {
        color: theme.palette.primary.main
    },
    iconbtn: {
        "&:hover": {
            border: "1px solid #FFF"
        }
    },

    componentDidMount() {
        document.title = ' داشبرد  ';
    }
}))

export default function HomeScreen() {
    const classes = useStyles();
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);
    const token = localStorage.getItem('access_token')
    console.log(token)
    useEffect(() => {
        setLoading(true)
        axios({
            url: 'http://elanza.ms/public/api/v1/orders/top-widgets',
            method: 'GET',
            timeout: 3000,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token
            },
        }).then(response => {
            if (response.status === 200) {
                console.log(response.data.data)
                setData(response.data.data)
            }
            setLoading(false)
        }).catch(error => {
            console.log(error)
        })
    }, [])

    return (
        <>
            <div className="container">
                <div className="row mb-4">
                    <NavBarComponent/>
                </div>
                <div className="row d-flex justify-content-around mb-4">
                    <div className="col-5  d-flex flex-column justify-content-center Total-Products">
                        <div className="row">
                            <div className="col-6 d-flex justify-content-start dashboard-number">
                                {data.total_products_count}
                            </div>
                            <div className="col-6 d-flex justify-content-end">
                                <i className="fal fa-boxes dashboard-icons"></i>
                            </div>
                        </div>
                        <div className="row d-flex justify-content-start pt-3 pr-3 dashboard-title">کل محصولات</div>

                    </div>
                    <div className="col-5 d-flex flex-column justify-content-center Consumer-Products">
                        <div className="row">
                            <div className="col-6 d-flex justify-content-start  dashboard-number">
                                {data.total_products_to_sell}
                            </div>
                            <div className="col-6 d-flex justify-content-end">
                                <i className="fal fa-box-check dashboard-icons"></i>
                            </div>
                        </div>
                        <div className="row d-flex justify-content-start pt-3 pr-3 dashboard-title">محصولات قابل فروش
                        </div>
                    </div>
                </div>
                <div className="row d-flex justify-content-around mb-4">
                    <div className="col-5  d-flex flex-column justify-content-center Finishing-Products">
                        <div className="row">
                            <div className="col-6 d-flex justify-content-start dashboard-number">
                                {data.products_are_about_end}
                            </div>
                            <div className="col-6 d-flex justify-content-end">
                                <i className="fal fa-box-open dashboard-icons"></i>
                            </div>
                        </div>
                        <div className="row d-flex justify-content-start pt-3 pr-3 dashboard-title">محصولات در حال اتمام
                        </div>

                    </div>
                    <div className="col-5 d-flex flex-column justify-content-center Total-orders">
                        <div className="row">
                            <div className="col-6 d-flex justify-content-start  dashboard-number">
                                {data.total_orders}
                            </div>
                            <div className="col-6 d-flex justify-content-end">
                                <i className="fal fa-bags-shopping dashboard-icons"></i>
                            </div>
                        </div>
                        <div className="row d-flex justify-content-start pt-3 pr-3 dashboard-title">کل سفارشات</div>
                    </div>
                </div>
                <div className="row d-flex justify-content-around mb-4">
                    <div className="col-5  d-flex flex-column justify-content-center Deliverd-Orders">
                        <div className="row">
                            <div className="col-6 d-flex justify-content-start  dashboard-number">
                                {data.shipped_orders}
                            </div>
                            <div className="col-6 d-flex justify-content-end">
                                <i className="fal fa-gifts dashboard-icons"></i>
                            </div>
                        </div>
                        <div className="row d-flex justify-content-start pt-3 pr-3 dashboard-title">سفارشات تحویل داده
                            شده
                        </div>

                    </div>
                    <div className="col-5 d-flex flex-column justify-content-center New-Orders">
                        <div className="row">
                            <div className="col-6 d-flex justify-content-start  dashboard-number">
                                {data.pending_orders}
                            </div>
                            <div className="col-6 d-flex justify-content-end">
                                <i className="fal fa-shopping-bag dashboard-icons"></i>
                            </div>
                        </div>
                        <div className="row d-flex justify-content-start pt-3 pr-3 dashboard-title">سفارشات جدید</div>
                    </div>
                </div>


                <footer>
                    <FooterComponent/>
                </footer>

            </div>
        </>
    )
}
