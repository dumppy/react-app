import React, {useState, useEffect, useContext} from 'react'
import {useParams, withRouter, Redirect} from 'react-router-dom'
import {makeStyles} from "@material-ui/core/styles"
import useKeyboard from '../components/useKeyboardEvent'
import alert from '../components/AlertComponent'
import axios from 'axios'

const useStyles = makeStyles(theme => ({
    root: {
        /*    display: "flex",
            justifyContent: "center",
            alignItems: "center",
            overflow: 'hidden'*/
    },

    componentDidMount() {
        document.title = '  بازیابی رمز عبور  ';
    }
}))

export default function VerifyScreen() {
    const classes = useStyles()
    const token = localStorage.getItem('access_token')
    console.log(token)
    const username = localStorage.getItem('username')
    const [code, setCode] = useState(' ')
    const [captcha, setCaptcha] = useState('')
    const [captchaEnter, setCaptchaEnter] = useState(' ')
    const [open, setOpen] = useState(false)
    const [data, setData] = useState([])
    const [status, setStatus] = useState(200)
    const [message, setMessage] = useState(' ')

    //keypress inputs by enter
    useKeyboard('Enter', (e) => {
        verify()
    })
    const verify = () => {
        let data = new FormData();
        data.append('code', code)

        setStatus('');

        axios({
            method: 'POST', url: 'http://elanza.ms/api/v1/users/verify', headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',

            },
            data: {
                email_or_mobile: '',
                code: code
            }
        })
            .then(res => {
                setData(res.data)
                console.log('dwd', res.data.status, res.data.data.access_token)
                if (res.data.status) {
                    setOpen(true)
                    setStatus(true);
                    localStorage.setItem('access_token', res.data.data.access_token)
                } else {
                    setStatus(false);
                    setMessage(res.data.data.messages)
                }
            })
            .catch(error => {
                if (error.response) {
                    console.log(error.response, error.response.status)
                    setStatus(error.response.status)
                }
            })
    }


    const codeNotSend = () => {

        setStatus('');
        axios({
            method: 'POST', url: 'http://elanza.ms/public/api/v1/users/otp', headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'X-Requested-With': 'XMLHttpRequest'
            },
            data: {
                email_or_mobile: username,
            }
        })
            .then(res => {
                setData(res.data)
                console.log(res.data.status, res.data.data.token)
                if (res.data.status) {
                    setOpen(true)
                    setStatus(true);
                } else {
                    setStatus(false);
                    setMessage(res.data.data.messages)
                }
            })
            .catch(error => {
                console.log(error.response, error.response.status)
                setStatus(error.response.status)
            })
    }

    const result = (text) => {
        setCaptcha(text)
    }

    const handleClose = () => {
        setOpen(false)
    }

    return (
        <>
            <div className="container">
                <div className="row d-md-none">
                    <div className="col-12 bg-mobile">
                        <img src="../assets/img/bg-mobile.png" className="d-block w-100 img-fluid"/>
                    </div>
                </div>

                <div className="row mb-3 title-background">
                    <div className="col-12 d-flex justify-content-start mb-1">
                        <p className="title underline-title pt-5 pb-3 mr-md-5">بازیابی رمز عبور</p>
                    </div>
                    <div className="col-12 d-flex justify-content-start mt-0 mb-4">
                        <small className="pb-3 mr-md-5">رمز عبور به ایمیل شما فرستاده شد، لطفا فرم زیر را پر
                            کنید.</small>
                    </div>

                    <div className="card-body col-md-8 d-block text-c   enter div-center d-md-block d-none">
                        <div className="col-12 form-group mb-4">
                            <label htmlFor="dode" className="d-flex justify-content-start input-label"> کد
                                دریافتی </label>
                            <input type="text" id="code" className="form-control"
                                   value={code} onChange={(e) => setCode(e.target.value)}
                            />
                        </div>

                        <button type="button" onClick={codeNotSend}
                                className="d-md-block d-none btn btn-register mr-3 px-md-5 py-2 mb-3 text-center">
                            ارسال مجدد کد
                        </button>

                    </div>

                    <button type="button" onClick={codeNotSend}
                            className="d-md-none btn btn-register btn-block w-100 py-2 mb-0">
                        ارسال مجدد کد
                    </button>
                    {alert(status)}
                </div>
            </div>
            <script src="js/jquery-3.3.1.slim.min.js"></script>
            <script src="js/popper.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/bootstrap-show-password.min.js"></script>
        </>
    )
}
