import React, { useState, useEffect } from 'react'
import axios from 'axios'
import ContentLoader from 'react-content-loader'

export default function ContactScreen() {
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(false)
    const MyLoader = () => (
        <ContentLoader viewBox="0 0 380 70">
            {/* Only SVG shapes */}
            <rect x="0" y="0" rx="4" ry="4" width="300" height="13" />
            <rect x="0" y="25" rx="4" ry="4" width="300" height="13" />
            <rect x="0" y="50" rx="4" ry="4" width="300" height="13" />
        </ContentLoader>
    )
    useEffect(() => {
        setLoading(true)
        axios({
            url: 'https://swapi.co/api/people',
            method: 'GET',
            timeout: 3000,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',

            },
        }).then(response => {
            if (response.status === 200) {
                console.log(response.data.results)
                setData(response.data.results)
            }
            setLoading(false)
        })
    }, [])
    return (
        <div style={{ width: 500 }} >
            {loading ? (
                <MyLoader />
            ) : (null)}
            {data.map((item) => <div key={data.id} style={{ color: "#fff" }} >{item.name}</div>)}
        </div>
    )
}