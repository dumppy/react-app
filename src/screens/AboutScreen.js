import React,{useState,useEffect} from 'react'
import { makeStyles } from "@material-ui/core/styles";
import Typography from '@material-ui/core/Typography';
import TelegramIcon from '@material-ui/icons/Telegram';
import IconButton from '@material-ui/core/IconButton';
import Title from '../components/Title'
import Modal from '@material-ui/core/Modal';
import LinearProgress from '@material-ui/core/LinearProgress';

import {useParams,withRouter,Redirect} from 'react-router-dom'
import MyAutoComplete from '../components/MyAutoComplete'

const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        flexDirection: "column",
        width: "100%",
        minHeight: "100vh",
        padding:50
    },
    name: {
        color: theme.palette.primary.main
    },
    iconbtn:{
        "&:hover":{
            border:"1px solid #FFF"
        }
    }

}));
export default function AboutScreen() {
    const[value,setValue]=useState(0)
    const classes = useStyles()
    const [modalOpen,setModalOpen]=[false]
    let {name} =useParams()

    useEffect(()=>{
        if(value===100){
            clearTimeout(timer)
            return
        }else{
           var timer= setTimeout(()=>{
                setValue(value=>value+1)
            },100)
        }
    },[value])
   
    return (
        <>
        <div className={classes.root} >
                <Title txt={name} />
                <LinearProgress
                style={{width:"60%"}} variant="determinate" value={value} />
                <MyAutoComplete />

        </div>
        <Modal open={modalOpen} onClose={()=>setModalOpen(false)} >
            <div style={{width:200,height:200,backgroundColor:"#Fff"}}>

            </div>
        </Modal>
        </>
    )
}
