import React, {useState, useEffect} from 'react'
import {useParams, withRouter, Redirect, Link} from 'react-router-dom'
import {makeStyles} from "@material-ui/core/styles"
import Typography from '@material-ui/core/Typography'
import TelegramIcon from '@material-ui/icons/Telegram'
import IconButton from '@material-ui/core/IconButton'
import Title from '../components/Title'
import MyAutoComplete from '../components/MyAutoComplete'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Alert from '@material-ui/lab/Alert';
import Snackbar from '@material-ui/core/Snackbar'
import axios from 'axios'
import NavBarComponent from "../components/NavBarComponent";
import alert from "../components/AlertComponent";

const useStyles = makeStyles(theme => ({
    root: {
        /*    display: "flex",
            justifyContent: "center",
            alignItems: "center",
            overflow: 'hidden'*/
    },

    componentDidMount() {
        document.title = 'ورود  ';
    }
}))

export default function LoginScreen() {
    const classes = useStyles()
    const [username, setUsername] = useState("ellsworth33@example.com")
    const [password, setPassword] = useState("123456")
    const [file, setFile] = useState("11111111")
    const token = localStorage.getItem('access_token')
    const [btnDisabled, setBtnDisabled] = useState(true)
    const [open, setOpen] = useState(false)
    const [data, setData] = useState([])
    const [status, setStatus] = useState(false)
    const [message, setMessage] = useState('')
    const meta = {
        title: 'Some Meta Title',
        description: 'I am a description, and I can create multiple tags',
        canonical: 'http://example.com/path/to/page',
        meta: {
            charset: 'utf-8',
            name: {
                keywords: 'react,meta,document,html,tags'
            }
        }
    };
    useEffect(() => {
        let Emailpattern = /^[a-z][a-z0-9]{2,}@[a-z]{3,}\.[a-z]{2,4}$/i
        if (Emailpattern.test(username)) {
            setBtnDisabled(false)
        } else {
            setBtnDisabled(true)
        }
    }, [username, password])
    const login = () => {
        let data = new FormData();
        data.append('username', username);
        data.append('password', password);

        axios({
            method: 'POST', url: 'http://elanza.ms/public/api/v1/users/login', headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'X-Requested-With': 'XMLHttpRequest'
            },
            data: {
                email_or_mobile: username,
                password: password,
            }
        })
            .then(res => {
                setData(res.data)
               // console.log(res.data.status, res.data.data.access_token)
                if (res.data.status) {
                    setOpen(true)
                    setMessage(res.data.data.message)
                    setStatus(true);
                    localStorage.setItem('access_token', res.data.data.access_token)
                } else {
                    setStatus(false);
                    setMessage(res.data.data.messages)
                }
            })
            .catch(error => {
                if (error.response) {
                   // console.log(error.response, error.response.status)
                    setStatus(error.response.status)
                }
            })
    }
    const handleClose = () => {

        setOpen(false)
    }
    console.log(status)
   if (token !== 'null')   return <Redirect to='/' />

    return (
        <>
            <div className='container'>
                <NavBarComponent/>
                <div className="row d-md-none">
                    <div className="col-12 bg-mobile">

                    </div>
                </div>

                <div className="row mb-3 title-background">
                    <div className="col-12 d-flex justify-content-start mb-3">
                        <p className="title underline-title pt-5 pb-3 mr-md-5">ورود به پنل فروشندگان الانزا</p>
                    </div>

                    <div className="card-body col-md-8 d-block text-center div-center d-md-block d-none">
                        <div className="col-12 form-group mb-4">
                            <label htmlFor="username" className="d-flex justify-content-start input-label">ایمیل یا
                                موبایل</label>
                            <input required value={username} onChange={(e) => setUsername(e.target.value)}
                                   type="text" id="username" className="form-control"/>
                        </div>

                        <div className="col-12 form-group mb-4">
                            <label id="password-label">رمز عبور</label>
                            <div className="input-group">
                                <input required value={password} onChange={(e) => setPassword(e.target.value)}
                                       type="password" className="form-control" id="password" data-toggle="password"/>
                            </div>
                        </div>

                        <div className="col-12 d-block d-md-none mb-4">
                            <Link to="/reset-password"
                                  className="forget-password d-flex justify-content-start">{'  رمز عبورم را فراموش کرده ام. '}
                            </Link>
                        </div>

                        <div className="col-12 d-flex justify-content-start mb-4">
                            <div className="form-check form-check-inline mr-0">
                                <input className="form-check-input" type="checkbox" name="inlineRadioOptions"
                                       id="remember-me"
                                       value="option1"/>
                                <label className="form-check-label mx-1" htmlFor="remember-me">مرا بخاطر بسپار </label>
                            </div>
                        </div>

                        <button onClick={login} disabled={btnDisabled} color="primary"
                                type="button"
                                className="d-md-block d-none btn btn-register mr-3 px-md-5 py-2 mb-3 text-center">
                            ورود
                        </button>

                        <div className="col-12 d-none d-md-block">
                            <Link to="/reset-password"
                                  className="forget-password d-fgit adddlex justify-content-start">{'  رمز عبورم را فراموش کرده ام. '}
                            </Link>
                        </div>

                    </div>

                    <button onClick={login} type="button"
                            className="d-md-none btn btn-register btn-block w-100 py-2 mb-0">
                        ورود
                    </button>
                </div>

                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'center',
                    }}
                    open={open}
                    autoHideDuration={500}
                    onClose={handleClose}
                    message="شما با موفقیت وارد شدید "
                />

            </div>
        </>
    )
}
