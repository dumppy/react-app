import React, { useState } from "react";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import IconButton from "@material-ui/core/IconButton";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import MailIcon from "@material-ui/icons/Mail";
import MenuIcon from "@material-ui/icons/Menu";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import ProfileImage from "../assets/img/profile.jpg";
import HomeScreen from "./HomeScreen";
import AboutScreen from "./AboutScreen";
import ResumeScreen from "./ResumeScreen";
import BlogScreen from "./BlogScreen";
import ContactScreen from "./ContactScreen";
import Button from "@material-ui/core/Button";
import {changeLanguage,lang,getTranslate} from '../locale/index'
import './index.css'
const drawerWidth = 300;

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  drawer: {
    [theme.breakpoints.up("sm")]: {
      width: drawerWidth,
      flexShrink: 0
    }
  },
  appBar: {
    [theme.breakpoints.up("sm")]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth
    }
  },
  menuButton: {
    marginRight: theme.spacing(2),
    position:'absolute',
    left:0,
    top:50,
    zIndex:1,
    [theme.breakpoints.up("sm")]: {
      display: "none"
    }
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
    backgroundColor: "#191d2b",
    borderRight: "1px solid #2e344e"
  },
  content: {
    flexGrow: 1,
    width:"100%",
    minHeight:"100vh",
    position:'relative'
  },
  TopDrawer: {
    width: "100%",
    height: 240,
    paddingTop: 20,
    paddingBottom: 20,
    borderBottom: "1px solid #2e344e",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  profileimage: {
    width: 200,
    height: 200,
    borderRadius: 100,
    border: "7px solid #2e344e"
  },
  ItemMenu: {
    textAlign: "center"
  },
  BottomDrawer: {
    height: 50,
    borderTop: "1px solid #2e344e",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
    
  },
  centerDrawer: {
    flex: 1,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width:"100%"
  }
}));

function IndexScreen(props) {
  const { container } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const [page, setPage] = useState(2);
  const translate=getTranslate()
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const drawer = (
    <>
      <div className={classes.TopDrawer}>
        <img src={ProfileImage} className={classes.profileimage} />
      </div>
      <Divider />
      <div className={classes.centerDrawer}>
        <List style={{width:"100%"}} >
          <ListItem className="item" style={{width:"100%",backgroundColor:page===0?"#037fff":"transparent",paddingLeft:0,paddingRight:0}} button onClick={() => setPage(0)}>
            <ListItemText style={{width:"100%"}} className={classes.ItemMenu} primary={translate.home} />
            <div className="overlay" />
          </ListItem>
          <ListItem className="item" style={{width:"100%",backgroundColor:page===1?"#037fff":"transparent",paddingLeft:0,paddingRight:0}} button onClick={() =>{
             setPage(1);
            setMobileOpen(false)
          }}>
            <ListItemText className={classes.ItemMenu} primary={translate.about}/>
            <div className="overlay" />
          </ListItem>
          <ListItem className="item" style={{width:"100%",backgroundColor:page===2?"#037fff":"transparent",paddingLeft:0,paddingRight:0}} button onClick={() => setPage(2)}>
            <ListItemText className={classes.ItemMenu} primary={translate.resume} />
            <div className="overlay" />
          </ListItem>
          <ListItem className="item" style={{width:"100%",backgroundColor:page===3?"#037fff":"transparent",paddingLeft:0,paddingRight:0}} button onClick={() => setPage(3)}>
            <ListItemText className={classes.ItemMenu} primary={translate.blog} />
            <div className="overlay" />
          </ListItem>
          <ListItem className="item" style={{width:"100%",backgroundColor:page===4?"#037fff":"transparent",paddingLeft:0,paddingRight:0}} button onClick={() => setPage(4)}>
            <ListItemText className={classes.ItemMenu} primary={translate.contact} />
            <div className="overlay" />
          </ListItem>
        </List>
      </div>

      <div className={classes.BottomDrawer}>
        <Button color={lang==='fa'?"primary":"secondary"}   onClick={()=>changeLanguage('fa')}   >فارسی</Button>
        {'/'}
        <Button color={lang==='en'?"primary":"secondary"}  onClick={()=>changeLanguage('en')} >English</Button>
      </div>
    </>
  );

  return (
    <div className={classes.root}>
      <CssBaseline />
      <IconButton
        color="inherit"
        aria-label="open drawer"
        edge="start"
        onClick={handleDrawerToggle}
        className={classes.menuButton}
      >
        <MenuIcon />
      </IconButton>

      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === "rtl" ? "right" : "left"}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper
            }}
            ModalProps={{
              keepMounted: true // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
      <main className={classes.content}>
        <span className="line1" ></span>
        <span className="line2" ></span>
        <span className="line3" ></span>
        <span className="line4" ></span>
        {page === 0 ? (
          <HomeScreen />
        ) : page === 1 ? (
          <AboutScreen />
        ) : page === 2 ? (
          <ResumeScreen />
        ) : page === 3 ? (
          <BlogScreen />
        ) : (
          <ContactScreen />
        )}
      </main>
    </div>
  );
}

export default IndexScreen;
