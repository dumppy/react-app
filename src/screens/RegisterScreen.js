import React, {useState} from "react";
import RCG from "react-captcha-generator";
import alert from "../components/AlertComponent";
import axios from "axios";

export default function RegisterScreen() {
    const [username, setUsername] = useState('')
    const [captcha, setCaptcha] = useState('')
    const [captchaEnter, setCaptchaEnter] = useState(' ')
    const [status, setStatus] = useState(200)
    const [message, setMessage] = useState('   فرستاده   شد')

    const register = () => {
        let data = new FormData();
        data.append('username', username);

        if (captcha.toLowerCase().trim() !== captchaEnter.toLowerCase().trim()) setStatus('captcha-not-valid')
        else {
            axios({
                method: 'POST', url: 'http://elanza.ms/public/api/v1/users/otp', headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'X-Requested-With': 'XMLHttpRequest'
                },
                data: {
                    email_or_mobile: username,
                }
            })
                .then(res => {
                    console.log(res.data.status, res.data.data.token)
                    if (res.data.status) {
                        setStatus(true);
                        localStorage.setItem('access_token', res.data.data.access_token)
                        localStorage.setItem('username', username)
                    } else {
                        setStatus(false);
                        setMessage(res.data.data.messages)
                    }
                })
                .catch(error => {
                    if (error.response) {
                        console.log(error.response, error.response.status)
                        setStatus(error.response.status)
                    }

                })
        }
    }

    return (
        <>
            <div className="container">
                <div className="row d-md-none">
                    <div className="col-12 bg-mobile">
                        <img src="../assets/img/bg-mobile.png" className="d-block w-100 img-fluid"/>
                    </div>
                </div>

                <div className="row mb-3 title-background">
                    <div className="col-12 d-flex justify-content-start mb-1">
                        <p className="title underline-title pt-5 pb-3 mr-md-5">ثبت نام </p>
                    </div>
                    <div className="col-12 d-flex justify-content-start mt-0 mb-4">
                        <small className="pb-3 mr-md-5">رمز عبور به ایمیل شما فرستاده خواهد شد، لطفا فرم زیر را پر
                            کنید.</small>
                    </div>

                    <div className="card-body col-md-8 d-block text-center div-center d-md-block d-none">

                        <div className="col-12 form-group mb-4">
                            <label htmlFor="username" className="d-flex justify-content-start input-label">ایمیل یا شماره موبایل </label>
                            <input type="text" id="username" className="form-control"
                                   value={username} onChange={(e) => setUsername(e.target.value)}
                            />
                        </div>

                        <div className="col-12 form-group mb-4">
                            <label htmlFor="captcha" className="d-flex justify-content-start input-label">تصویر
                                امنیتی</label>
                            <input type="text" id="captchaEnter" className="form-control" value={captchaEnter}
                                   onChange={(e) => setCaptchaEnter(e.target.value)}/>
                            <RCG result={setCaptcha()}/>
                            <i className="fa fa-refresh" aria-hidden="true"> </i>
                        </div>

                        <button type="button" onClick={register}
                                className="d-md-block d-none btn btn-register mr-3 px-md-5 py-2 mb-3 text-center">
                            ثبت نام
                        </button>

                    </div>

                    <button type="button" className="d-md-none btn btn-register btn-block w-100 py-2 mb-0">
                        ثبت نام
                    </button>
                    {alert(status, message)}
                </div>
            </div>
            <script src="js/jquery-3.3.1.slim.min.js"></script>
            <script src="js/popper.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/bootstrap-show-password.min.js"></script>
        </>
    );
}