import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Test from '../components/Test'
const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        padding: 60
    },
    button: {
        marginTop: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    actionsContainer: {
        marginBottom: theme.spacing(2),
    },
    resetContainer: {
        padding: theme.spacing(3),
    },
}));

export default function VerticalLinearStepper() {
    const classes = useStyles();
    const [activeStep, setActiveStep] = React.useState(0);
    const steps = [
        { id: 0, title: "step 1", desc: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quas, magni mollitia, aspernatur consequatur accusamus vero eum facere exercitationem velit suscipit ipsam placeat libero. Deleniti exercitationem nostrum quasi. Molestiae, vel porro.", subtitle: "" },
        { id: 1, title: "step 2", desc: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quas, magni mollitia, aspernatur consequatur accusamus vero eum facere exercitationem velit suscipit ipsam placeat libero. Deleniti exercitationem nostrum quasi. Molestiae, vel porro.", subtitle: "" },
        { id: 2, title: "step 3", desc: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quas, magni mollitia, aspernatur consequatur accusamus vero eum facere exercitationem velit suscipit ipsam placeat libero. Deleniti exercitationem nostrum quasi. Molestiae, vel porro.", subtitle: "" },
    ]

    return (
        <div className={classes.root}>
            <Stepper style={{ backgroundColor: 'transparent' }} orientation="vertical">
                {steps.map((step) =>
                    <Step key={step.id} active={true} >
                        <StepLabel>{step.title}</StepLabel>
                        <StepContent> {step.desc} </StepContent>
                    </Step>
                )}
            </Stepper>
            <Test >
                    <span >tets</span>
            </Test>
        </div>
    );
}