import en_us from './en'
import fa_ir from './fa'
const direction={
    fa:"rtl",
    en:"ltr"
}
const fonts={
    fa:"IRANSans",
    en:"Muli"
}
const translates={
    fa:fa_ir,
    en:en_us
}
const lang=localStorage.getItem('lang')?localStorage.getItem('lang'):"fa"
export {lang}
function getDirection(){
    return direction[lang]
}
export {getDirection}

function getFont(){
    return fonts[lang]
}
export {getFont}
function getTranslate(){
    return translates[lang]
}
export {getTranslate}

function changeLanguage(newLang){
    if(newLang===lang){
        return
    }
    localStorage.setItem('lang',newLang)
    window.location.reload()
}
export {changeLanguage}
