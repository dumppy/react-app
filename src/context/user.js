import React, {useEffect, useState, useContext,} from 'react'

export const userDefault = {
    username: '',
    userType: 'email',
    loggedIn: false
};
const userContext = React.createContext(userDefault)

export const UserProvider = userContext.Provider
export const UserConsumer = userContext.Consumer

export default userContext
