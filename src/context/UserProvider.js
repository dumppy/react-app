import React, {createContext, useContext, useReducer, useState} from 'react';
import userDefault from './user'

const StoreContext = createContext(userDefault);
const initialState = {count: 0, message: "", username: ''};

const reducer = (state, action) => {
    switch (action.type) {
        case "increment":
            return {
                count: state.count + 1,
                message: action.message
            }
        case "decrement":
            return {
                count: state.count - 1,
                message: action.message
            }
        case "reset":
            return {
                count: 0,
                message: action.message
            }
        default:
            throw new Error(`Unhandled action type: ${action.type}`);
    }
}

export const UserProvider = ({children}) => {
    //const [state, dispatch] = useReducer(reducer, initialState);
    const [user, setUser] = useState(userDefault);
    return (
        <StoreContext.Provider value={{...user, setUser}}>
            {children}
        </StoreContext.Provider>
    )
}

export const useUser = () => useContext(StoreContext);